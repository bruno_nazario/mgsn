import csv
import pandas as pd
from random import randint


# noinspection PyAttributeOutsideInit
class Mgsn:

    def __init__(self, input_file, output_path, param='peso'):
        self.input_file = input_file
        self.output_path = output_path

        self.__regras()

        self.__read_file()
        self.__generate_dicts()
        self.__generate_lists(param)

        self.games = list()

    def __regras(self):
        self.quadrantes = {
            3: 5311,
            4: 4689
        }

        keys = list()
        values = list()
        counter = 0
        for key in self.quadrantes.keys():
            for value in range(self.quadrantes.get(key)):
                keys.append(counter + 1)
                values.append(key)

                counter += 1

        self.quadrantes = dict(zip(keys, values))

        self.linhas = {
            3: 1839,
            4: 5276,
            5: 2886
        }

        keys = list()
        values = list()
        counter = 0
        for key in self.linhas.keys():
            for value in range(self.linhas.get(key)):
                keys.append(counter + 1)
                values.append(key)

                counter += 1

        self.linhas = dict(zip(keys, values))

    def __read_file(self):
        self.raw_base = pd.read_csv(self.input_file, sep=';', decimal=',')

    def __generate_dicts(self):
        self.linha_dezena = dict(zip(list(self.raw_base['dezena']), list(self.raw_base['linha'])))
        self.quadrante_dezena = dict(zip(list(self.raw_base['dezena']), list(self.raw_base['quadrante'])))

    def __generate_lists(self, param):
        self.maus = self.raw_base.sort_values(by=[param], ascending=[True], inplace=False)
        self.maus = list(self.maus['dezena'])
        self.maus = self.maus[:int(len(self.maus) / 2)]
        self.bons = self.raw_base.sort_values(by=[param], ascending=[False], inplace=False)
        self.bons = list(self.bons['dezena'])
        self.bons = self.bons[:int(len(self.bons) / 2)]

    def generate_games(self, jogos=(10, 11), pareto=(.8, .2), threshold=100):
        for jogo in jogos:
            for t in range(threshold):
                game = list()

                linhas = randint(1, 10000)
                linhas = self.linhas.get(linhas)
                linhas_usadas = set()

                quadrantes = randint(1, 10000)
                quadrantes = self.quadrantes.get(quadrantes)
                quadrantes_usados = set()

                for dezena in range(jogo):
                    dezenas = self.bons if (dezena + 1) / jogo <= pareto[0] else self.maus

                    while True:
                        dez = dezenas[randint(0, 29)]
                        if dez not in game:
                            if len(linhas_usadas) < linhas:
                                if len(quadrantes_usados) < quadrantes:
                                    linhas_usadas.update([self.linha_dezena.get(dez)])
                                    game.append(dez)
                                    break
                                else:
                                    _quadrantes_usados = set(quadrantes_usados)
                                    _quadrantes_usados.update([self.quadrante_dezena.get(dez)])
                                    if len(_quadrantes_usados) == quadrantes:
                                        quadrantes_usados.update([self.quadrante_dezena.get(dez)])
                                        game.append(dez)
                                        break
                            else:
                                _linhas_usadas = set(linhas_usadas)
                                _linhas_usadas.update([self.linha_dezena.get(dez)])
                                if len(_linhas_usadas) == linhas:
                                    if len(quadrantes_usados) < quadrantes:
                                        linhas_usadas.update([self.linha_dezena.get(dez)])
                                        game.append(dez)
                                        break
                                    else:
                                        _quadrantes_usados = set(quadrantes_usados)
                                        _quadrantes_usados.update([self.quadrante_dezena.get(dez)])
                                        if len(_quadrantes_usados) == quadrantes:
                                            quadrantes_usados.update([self.quadrante_dezena.get(dez)])
                                            game.append(dez)
                                            break

                self.games.append(sorted(game))

    def export(self):
        with open('%s/%s' % (self.output_path, 'mgsn-out.csv'), 'w', newline='') as f:
            w = csv.writer(f, delimiter=';')
            w.writerows(self.games)

if __name__ == '__main__':
    params = ['maximo', 'media', 'minimo', 'jogos', 'dias', 'peso']
    mgsn = Mgsn('C:/Dev/mgsn/lib/mgsn.csv', 'C:/Dev/mgsn/lib/out')

    mgsn.generate_games(jogos=tuple(x for x in range(7, 12)), pareto=(.7, .3), threshold=7)
    mgsn.export()
